<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json(User::showAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // common validation
        $rules = array(
            'name' => 'required|max:32',
            'type' => 'required|in:'.implode(',',User::$TYPES)
        );
        $validator = Validator::make($request->post(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422);
        }

        $rules = [];

        // specific validation
        if($request->post('type') == User::$TYPES['snmp']){
            $rules = ['passwordA' => 'required|min:5', 'passwordB' => 'required|min:5'];
        }
        elseif($request->post('type') == User::$TYPES['local']){
            $rules = ['password' => 'required|min:5'];
        }
        if(! empty($rules)){
            $validator = Validator::make($request->post(), $rules);
            if ($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()
                ], 422);
            }
        }

        // insert & response
        return User::createUser($request->post());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //dd(__METHOD__);
    }

    public function showOne( int $userId)
    {
        /**
         * @var User $user
         */
        $user = User::findOrFail($userId);
        return response()->json(User::showOne($user));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {


       // common validation
        $rules = array(
            'name' => 'required|max:32',
            'enabled' => 'required|bool'
        );
        $validator = Validator::make($request->post(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422);
        }

        $rules  = []; // init rule


        if($user->type == User::$TYPES['snmp']){

            $inputs = Arr::only($request->post(), ['name','enabled','passwordA','passwordB']);

            if( ! array_key_exists('passwordA', $inputs)){
                $inputs['passwordA'] = '';
            }

            if( ! array_key_exists('passwordB', $inputs)){
                $inputs['passwordB'] = '';
            }

            if( !empty($inputs['enabled'])){
                $rules = ['passwordA' => 'required:min:1', 'passwordB' => 'required|min:1|max:32'];
            }

        }
        elseif($user->type == User::$TYPES['local']){

            $inputs = Arr::only($request->post(), ['name','enabled', 'password']);

            if( ! array_key_exists('password', $inputs)){
                $inputs['password'] = '';
            }

            if( !empty($inputs['enabled'])){
                $rules = ['password' => 'required|min:1|max:32'];
            }
        }
        else{
            $inputs = Arr::only($request->post(), ['name','enabled']);
        }

        if( ! empty($rules)){
            $validator = Validator::make($inputs, $rules);
            if ($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()
                ], 422);
            }
        }

        // insert & response
        return User::updateUser($inputs, $user);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response(null, 204);
    }


    public function convertType(Request $request, int $userId)
    {
        /**
         * @var User $user
         */
        $user = User::findOrFail($userId);

        $rules = array(
            'type' => 'required|in:'.implode(',',User::$TYPES)
        );
        $validator = Validator::make($request->post(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422);
        }


        //echo " $user->type  => ".$request->post('type');
        /**
         * @desc community to local or snmp
         */
        if($user->type === User::$TYPES['community'] && in_array($request->post('type'), [User::$TYPES['local'], User::$TYPES['snmp']])){

            foreach (['password','passwordA','passwordB'] as $field){
                $user->{$field} = '';
            }
            $user->enabled = false;
        }
        /**
         * @desc snmp to local
         */
        elseif($user->type === User::$TYPES['snmp'] && in_array($request->post('type'), [User::$TYPES['local']])){

            $user->password  = $user->passwordA;
            $user->passwordA = '';
            $user->passwordB = '';
        }
        /**
         * @desc local to snmp
         */
        elseif($user->type === User::$TYPES['local'] && in_array($request->post('type'), [User::$TYPES['snmp']])){
            $user->passwordA = $user->password;
            $user->passwordB = $user->password;
            $user->password  = '';
        }

        else  {
            return response()->json(['error'=>'convert '.$user->type.' to '.$request->post('type').'  not allowed'], 400);
        }


        $user->type = $request->post('type');
        $user->save();
        return response(null, 204);
    }

}
