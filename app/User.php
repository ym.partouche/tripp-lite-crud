<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $type
 * @property bool $enabled
 * @property string password
 * @property string passwordA
 * @property string passwordB
 * @property int $id
 *
 */
class User extends Model
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = ['password', 'passwordA', 'passwordB'];
    /**
     * @var array
     */
    protected $guarded = ['id'];
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public static $TYPES = [
        'community' => 'Community',
        'snmp' => 'SNMP',
        'local' => 'Local'
    ];

    private static $hidePassword=false;
    /**
     * @return array
     */
    public static function showAll() {
        $output = [];
        foreach (self::all() as $user){
            if(self::$hidePassword){
                foreach (['password','passwordA','passwordB'] as $field){
                    if(!empty($user->{$field})){
                        $user->{$field} = '******';
                    }
                }
            }

            $output[] = $user;
        }

        return $output;
    }

    /**
     * @param User $user
     * @return User
     */
    public static function showOne(User $user) {

        if(self::$hidePassword){
            foreach (['password','passwordA','passwordB'] as $field){
                if(!empty($user->{$field})){
                   $user->{$field} = '******';
                }
            }
        }

        return $user;
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\JsonResponse
     */
    public static function createUser(array $params) {

        $params['enabled'] = true;

        foreach (['password','passwordA','passwordB'] as $field){
            if(array_key_exists($field ,$params)){
                $params[$field] = md5($params[$field]);
            }
        }


        try{
            $model = User::create($params);
            return response()->json(['id'=>$model->id]);
        } catch (\Exception $e) {
            return response()->json(['error'=>$e->getMessage()], 422);
        }


    }

    /**
     * @param array $inputs
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public static function updateUser(array $inputs, User $user)
    {
        foreach (['password','passwordA','passwordB'] as $field){
            if(array_key_exists($field ,$inputs) && !empty($inputs[$field])){
                $inputs[$field] = md5($inputs[$field]);
            }
        }
        //dd($inputs);
        try{
            $user->update($inputs);

            return response()->json(null, 204);

        } catch (\Exception $e) {
            return response()->json(['error'=>$e->getMessage()], 422);
        }
    }
}
