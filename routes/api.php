<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


/**
 * @desc CRUD on user resource
 */
Route::resource('users', 'UserController')->only([
    'index', 'store', 'update', 'destroy','delete'
]);

// show user detail
Route::get('/users/{id}', ['as' => 'show', 'uses' => 'UserController@showOne']);
/**
 * @desc convert user type
 */
Route::patch('/users/{id}/convert', ['as' => 'convert', 'uses' => 'UserController@convertType']);
